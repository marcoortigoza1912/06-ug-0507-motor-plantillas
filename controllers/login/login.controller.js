const {db} = require("../../db/conexion");


const loginController = {
    logout: function(req, res) {
        req.session.destroy((err) => {
            if (err) {
                console.error(err);
                res.status(500).send("Error logging out");
            } else {
                res.redirect("/index.html");
            }
        });
    }
}

module.exports = loginController;
