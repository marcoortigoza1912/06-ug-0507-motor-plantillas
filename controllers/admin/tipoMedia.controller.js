
const express = require('express');
const { getALL, db} = require ("../../db/conexion");
const tipomediaStoreSchema = require("../../validators/tipomedia/create");
const tipomediaEditSchema = require("../../validators/tipomedia/edit");
const multer = require('multer');
const { update, destroy } = require('./integrantes.controller');
const { required } = require('joi');
const TipoMedioModel = require("../../models/tipomedio.model");


const tipoMediaController = {
    index: async function(req, res) {
      try {
        let nombre = req.query?.s?.nombre || '';
  
        console.log("datos de búsqueda", nombre);
  
        const filters = [];
        if (nombre) {
          filters.push(`nombre LIKE "%${nombre}%"`);
        }
  
        const tipomedia = await TipoMedioModel.getAll(filters);
  
        if (!tipomedia || tipomedia.length === 0) {
          return res.render("admin/tipo_media/index", {
            tipomedia: [],
            error: 'No se encontraron coincidencias.'
          });
        }
  
        res.render("admin/tipo_media/index", {
          tipomedia: tipomedia,
          error: null
        });
      } catch (error) {
        console.error("Error al ejecutar la consulta:", error);
        res.status(500).render("error");
      }
    },
  
    create: async function(req, res) {
      const mensaje = req.query.mensaje;
      res.render('admin/tipo_media/formularioTipoMedia', {
        mensaje: mensaje
      });
    },
  
    store: async function(req, res) {
      try {
        const { error } = tipomediaStoreSchema.validate(req.body);
  
        if (error) {
          const mensajeError = error.details.map(detail => detail.message).join('. ');
          return res.redirect(`/admin/tipo_media/crear?mensaje=${encodeURIComponent(mensajeError)}`);
        }
  
        const { nombre, borralog } = req.body;
  
        // Verificar si el nombre ya existe en la base de datos
        const count = await TipoMedioModel.nombreduplicado(nombre);
        if (count > 0) {
          return res.redirect(`/admin/tipo_media/crear?mensaje="El nombre '${nombre}' ya existe. Favor elegir otro"&nombre=${nombre}&borralog=${borralog}`);
        }
  
        // Obtener el máximo orden de la base de datos
        const maxOrden = await TipoMedioModel.getMaxOrden();
        const nuevoOrden = maxOrden + 1;
  
        // Insertar el nuevo tipo de medio en la base de datos
        await TipoMedioModel.insert(nombre, borralog, nuevoOrden);
  
        res.redirect("/admin/tipo_media/listar");
      } catch (error) {
        console.error("Error en la validación o en la inserción en la base de datos:", error);
        res.sendStatus(500);
      }
    },
  
    show() {},
  
    update: async function(req, res) {
      const idTipoMedia = req.params.idTipoMedia;
      console.log("idTipoMedia", idTipoMedia);
  
      // Validar los datos con Joi
      const { error } = tipomediaEditSchema.validate(req.body);
  
      if (error) {
        const mensajeError = error.details.map(detail => detail.message).join('. ');
        return res.redirect(`/admin/tipo_media/edit/${idTipoMedia}?mensaje=${encodeURIComponent(mensajeError)}`);
      }
  
      const nombre = req.body.nombre;
      const borralog = req.body.borralog || 'valor_predeterminado';
  
      try {
        const count = await TipoMedioModel.nombreduplicado(nombre, idTipoMedia);
        if (count > 0) {
          return res.redirect(`/admin/tipo_media/edit/${idTipoMedia}?mensaje=El nuevo nombre ya está en uso`);
        }
  
        await TipoMedioModel.update(idTipoMedia, nombre, borralog);
  
        res.redirect("/admin/tipo_media/listar?mensaje=Tipo Media editado exitosamente");
      } catch (error) {
        console.error("Error al actualizar la base de datos tabla Tipo Media:", error);
        res.sendStatus(500);
      }
    },
  
    edit: async function(req, res) {
      const idTipoMedia = req.params.idTipoMedia;
      console.log("idTipoMedia", idTipoMedia);
      console.log("Datos del formulario:", req.body);
  
      try {
        const tipomedia = await TipoMedioModel.getById(idTipoMedia);
        if (!tipomedia) {
          return res.status(404).render("error", { message: "Tipo de medio no encontrado" });
        }
  
        res.render("admin/tipo_media/editFormTipoMedia", {
          idTipoMedia,
          tipomedia,
        });
      } catch (error) {
        console.error("Error:", error);
        res.sendStatus(500);
      }
    },
  
    destroy: async function(req, res) {
      const idTipoMedia = req.params.idTipoMedia;
      console.log("idTipoMedia", idTipoMedia);
  
      try {
        const count = await TipoMedioModel.nombreduplicado(idTipoMedia);
        if (count > 0) {
          return res.redirect("/admin/tipo_media/listar?message=No se puede eliminar el tipo Media porque existen registros asociados en la tabla media");
        }
  
        await TipoMedioModel.destroy(idTipoMedia);
  
        res.redirect("/admin/tipo_media/listar?message=Tipo Media eliminado exitosamente");
      } catch (error) {
        console.error("Error al actualizar la base de datos tabla Tipo Media:", error);
        res.sendStatus(500);
      }
    }
  };
  
module.exports = tipoMediaController;