const { getALL, db} = require ("../../db/conexion");
const integrantesStoreSchema = require('../../validators/integrantes/create');
const integranteEditSchema = require("../../validators/integrantes/edit");
const IntegranteModel = require("../../models/integrante.model");

const IntegrantesController = {
    index: async function(req, res) {
        const filters = Object.entries(req.query.s || {}).filter(([key, value]) => value).map(([key, value]) => {
            if (Array.isArray(value)) {
                return `${key} IN ('${value.join("','")}')`;
            } else {
                return `${key} LIKE '%${value}%'`;
            }
        });
  
        try {
            const integrantesFiltrados = await IntegranteModel.getAll(filters);
            console.log("Sql Query", integrantesFiltrados);
  
            if (integrantesFiltrados.length === 0) {
                res.render("admin/integrantes/index", {
                    error: "No existen coincidencias.",
                    integrantes: [],
                });
            } else {
                res.render("admin/integrantes/index", {
                    integrantes: integrantesFiltrados,
                });
            }
        } catch (error) {
            console.error("Error:", error);
            res.status(500).send("Error interno del servidor");
        }
    },
    create: async function(req, res) {
        try {
            const mensaje = req.query.mensaje || '';
            const matricula = req.query.matricula || '';
            const nombre = req.query.nombre || '';
            const apellido = req.query.apellido || '';
            const borralog = req.query.borralog || '';
            
            res.render('admin/integrantes/crearFormulario', {
                mensaje: mensaje,
                integrante: {
                    matricula: matricula,
                    nombre: nombre,
                    apellido: apellido,
                    borralog: borralog
                }
            });
        } catch (error) {
            console.error("Error en la creación de integrante:", error);
            res.status(500).render("error");
        }
    },
    
    store: async function(req, res) {
        try {
            const { error } = integrantesStoreSchema.validate(req.body);

            if (error) {
                const mensajeError = error.details.map(detail => detail.message).join('. ');
                return res.redirect(`/admin/integrantes/crear?mensaje=${encodeURIComponent(mensajeError)}&matricula=${req.body.matricula}&nombre=${req.body.nombre}&apellido=${req.body.apellido}&borralog=${req.body.borralog}`);
            }

            const { matricula, nombre, apellido, borralog } = req.body;

            const maxOrden = await IntegranteModel.getMaxOrden();
            const nuevoOrden = maxOrden + 1;

            await IntegranteModel.create(matricula, nombre, apellido, borralog, nuevoOrden);

            res.redirect("/admin/integrantes/listar");
        } catch (error) {
            console.error("Error en la validación:", error);
            res.status(500).render("error");
        }
    },
  
    show: function() {},
  
    update: async function(req, res) {
        const idIntegrante = req.params.idIntegrante;
        console.log("idIntegrante", idIntegrante);

        try {
            const { error } = integranteEditSchema.validate(req.body);

            if (error) {
                const mensajeError = error.details.map(detail => detail.message).join('. ');
                return res.redirect(`/admin/integrantes/edit/${req.params.idIntegrante}?mensaje=${encodeURIComponent(mensajeError)}`);
            }

            const { nombre, apellido, borralog } = req.body;

            await IntegranteModel.update(idIntegrante, nombre, apellido, borralog);

            res.redirect("/admin/integrantes/listar?message=Integrante actualizado exitosamente");
        } catch (error) {
            console.error("Error en la validación o en la actualización:", error);
            res.status(500).render("error");
        }
    },
  
    edit: async function(req, res) {
      const idIntegrante = req.params.idIntegrante;
      console.log("idIntegrante", idIntegrante);
  
      try {
        const integrante = await IntegranteModel.getById(idIntegrante);
  
        if (!integrante) {
          return res.status(404).render("error");
        }
  
        res.render("admin/integrantes/editFormIntegrante", {
          idIntegrante: idIntegrante,
          integrante: integrante,
        });
      } catch (error) {
        console.error("Error:", error);
        res.status(500).send("Error interno del servidor");
      }
    },
  
    destroy: async function(req, res) {
      const idIntegrante = req.params.idIntegrante;
  
      try {
        const count = await IntegranteModel.countByField(idIntegrante);
  
        if (count > 0) {
          return res.redirect("/admin/integrantes/listar?message=No se puede eliminar el integrante porque existen registros asociados en la tabla media");
        }
  
        await IntegranteModel.delete(idIntegrante);
  
        res.redirect("/admin/integrantes/listar?message=Integrante eliminado exitosamente");
      } catch (error) {
        console.error("Error:", error);
        res.status(500).send("Error interno del servidor");
      }
    }
  };
module.exports = IntegrantesController;