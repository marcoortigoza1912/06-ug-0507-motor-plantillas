
const express = require('express');
const { getALL, db } = require('../../db/conexion'); 
const mediaStoreSchema = require("../../validators/media/create");
const mediaEditSchema = require('../../validators/media/edit');
const multer = require('multer');
const { EMPTY } = require("sqlite3");
const upload = multer({ dest: "../../public/assets/images/"});
const fileUpload = upload.single("url");//nombre de columna imagen
const fs = require("fs");
const MediaModel = require("../../models/media.model")



const MediaController= {
    index: async function (req, res) {
        const filters = Object.entries(req.query.s || {}).filter(([key, value]) => value).map(([key, value]) => {
            if (Array.isArray(value)) {
                return `${key} IN ('${value.join("','")}')`;
            } else {
                return `${key} LIKE '%${value}%'`;
            }
        });
        const filterQuery = filters.length ? `AND ${filters.join(" AND ")}` : "";
    
        try {
            const mediaFiltrados = await MediaModel.getAll(filters);
            console.log("Sql Query", mediaFiltrados);
    
            if (mediaFiltrados.length === 0) {
                res.render("admin/media/index", {
                    error: "No existen coincidencias.",
                    media: [], 
                });
            } else {
                res.render("admin/media/index", {
                    media: mediaFiltrados,
                });
            }
        } catch (error) {
            console.error("Error:", error);
            res.status(500).send("Error interno del servidor");
        }
    },
    create: async function(req, res) {
        const mensaje = req.query.mensaje;

        try {
            const matriculas = await MediaModel.getAllmatriculas();
            const nombresTipoMedia = await MediaModel.getAllTipoMedia();

            const media = {
                titulo: req.query.titulo || '',
                matricula: req.query.matricula || '',
                borralog: req.query.borralog || '',
                nombreSeleccionado: req.query.nombre || ''
            };

            res.render('admin/media/formularioMedia', {
                mensaje: mensaje,
                matriculas: matriculas,
                nombresTipoMedia: nombresTipoMedia,
                media: media
            });
        } catch (error) {
            console.error('Error al obtener las matrículas o los tipos de media:', error);
            res.status(500).render("error");
        }
    },
    
    store: async function(req, res) {
        try {
            // Validar los datos de creación de media con Joi
            const { error } = mediaStoreSchema.validate(req.body);
    
            if (error) {
                const mensajeError = error.details.map(detail => detail.message).join('. ');
                return res.redirect(`/admin/media/crear?mensaje=${encodeURIComponent(mensajeError)}`);
            }
    
            let mediaData = {
                nombre: req.body.nombre,
                titulo: req.body.titulo,
                borralog: req.body.borralog,
                matricula: req.body.matricula,
                orden: req.body.orden,
                tipo_media_id: req.body.tipo_media_id
            };
    
            // Agregar el URL según el tipo de medio (archivo o texto)
            if (req.file) {
                mediaData.url = req.file ? req.body.url + req.file.originalname : req.body.url;
            } else if (req.body.texto) {
                mediaData.url = req.body.texto;
            }
    
            // Insertar el medio utilizando el modelo
            await MediaModel.insert(mediaData);
    
            res.redirect('/admin/media/listar?success=' + encodeURIComponent('¡Registro insertado correctamente!'));
        } catch (error) {
            console.error('Error:', error);
            res.sendStatus(500);
        }
    },
    
    show(){},
    update: async function(req, res) {
        const idMedia = req.params.idMedia;
        console.log("idMedia", idMedia);
    
        try {
            const { error } = mediaEditSchema.validate(req.body);
    
            if (error) {
                const mensajeError = error.details.map(detail => detail.message).join('. ');
                return res.redirect(`/admin/media/edit/${req.params.idMedia}?mensaje=${encodeURIComponent(mensajeError)}`);
            }
    
            const { titulo, borralog } = req.body;
    
            // Realizar la actualización en el modelo
            await MediaModel.update(idMedia, req.body);
    
            res.redirect("/admin/media/listar?mensaje=El registro de media ha sido actualizado correctamente");
        } catch (error) {
            console.error("Error en la validación o en la actualización:", error);
            res.status(500).render("error");
        }
    },
    
    edit: async function(req, res) {
        const idMedia = req.params.idMedia;
        console.log("idMedia", idMedia);
    
        try {
            const media = await MediaModel.getById(idMedia);
            if (!media) {
                return res.status(404).render("error");
            } else {
                console.log("Datos del medio:", media);
                res.render("admin/media/editFormMedia", {
                    idMedia: idMedia,
                    media: media,
                });
            }
        } catch (error) {
            console.error("Error:", error);
            res.status(500).render("error");
        }
    },
    destroy: async function(req, res) {
        const idMedia = req.params.idMedia;
    
        try {
            await MediaModel.destroy(idMedia);
            res.redirect("/admin/media/listar?message=Media eliminado exitosamente");
        } catch (error) {
            console.error("Error al actualizar la base de datos:", error);
            res.status(500).send("Error interno del servidor");
        }
    }
};

module.exports = MediaController;