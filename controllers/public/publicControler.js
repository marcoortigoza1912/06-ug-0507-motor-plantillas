const express = require("express");
const router = express.Router();
//const db = require("../db/data")
//const dbsqlite = require("../db/conexion");
const { getALL,db} = require ("../../db/conexion");
const IntegranteModel = require("../../models/integrante.model");
const bcrypt = require('bcrypt');
const saltRounds = 10;



const PublicController = {
    index:async function (req, res) {
        const integrantes = await getALL("SELECT * FROM integrantes where borralog = 0");
    console.log(integrantes);
    // Definir la matrícula del home
    const matriculaDeHome = "Home";
    // Obtener los datos del home desde la base de datos
    const Home = await getALL(`SELECT * FROM home `);
    console.log("DatosDeHome", Home);
    // Renderizar la página index 
    res.render("index", {
        inicio: Home,
        integrantes: integrantes,
        //se trae los datos para el footer desde .env
        ENLACE: process.env.ENLACE,
        NOMBRE: process.env.NOMBRE,
        APELLIDO: process.env.APELLIDO,
        MATERIA: process.env.MATERIA
    });
    },
    index_integrantes: async function (req, res){
        const matricula = req.params.matricula;                                     
    const integrante = await getALL(`SELECT * FROM integrantes`);
    const datos = await getALL(`SELECT * FROM media WHERE matricula = '${matricula}'`);
    if (integrante.length === 0) {
        res.status(404).render("error");
    } else {
        res.render("integrante", {
            
            data: datos,
            integrantes: integrante,    
            ENLACE: process.env.ENLACE,
            NOMBRE: process.env.NOMBRE,
            APELLIDO: process.env.APELLIDO,
            MATERIA: process.env.MATERIA
        });
    }
    },
    index_worldCloud: async function(req, res){
        const integrante = await getALL(`SELECT * FROM integrantes`);
    res.render("word_cloud",{
        integrantes: integrante,
        ENLACE: process.env.ENLACE,
        NOMBRE: process.env.NOMBRE,
        APELLIDO: process.env.APELLIDO,
        MATERIA: process.env.MATERIA
    })
    },
    index_Curso: async function(req, res){
        const integrante = await getALL(`SELECT * FROM integrantes`);
    res.render("curso",{
        integrantes: integrante,
        ENLACE: process.env.ENLACE,
        NOMBRE: process.env.NOMBRE,
        APELLIDO: process.env.APELLIDO,
        MATERIA: process.env.MATERIA
    });
    },
    loginAutenticacion: function (req, res){
        const { usuario, contrasenha } = req.body;
    
        if (!usuario || !contrasenha) {
            return res.status(400).send("Por favor, ingresa tu correo electrónico y contraseña.");
        }
    
        db.get(
            `SELECT * FROM usuarios WHERE email = ?`,
            [usuario],
            async (err, db_usuario) => {
                if (err) {
                    console.log(err);
                    return res.status(500).send('Error interno del servidor');
                }
                try {
                    if (!db_usuario) {
                        return res.status(404).send("El correo electrónico ingresado no está registrado.");
                    }
                    
                    const contrasenhaValida = await bcrypt.compare(contrasenha, db_usuario.contraseña); // Corrección aquí
    
                    if (contrasenhaValida) {
                        req.session.logueado = {
                            id: db_usuario.id,
                            email: db_usuario.email,
                            sys_admin: db_usuario.superadmin, 
                        };
      
                        res.redirect('/admin');
                    } else {
                        res.status(401).send("La contraseña ingresada es incorrecta.");
                    }
                } catch (error) {
                    console.log(error);
                    res.status(500).send("Error interno del servidor");
                }
            }
        );
    },
    
crear_usuario: async function(req, res){
    try {
        const matriculas = await IntegranteModel.getAll(req);
        res.render('login/crearUsuario', {
            matricula: matriculas
        });
    } catch (error) {
        console.log(error);
        res.status(500).send("Error interno del servidor");
        return;
    }
    },
    guardar_usuario: async function(req, res) {
        const { email, password, matricula } = req.body;
    
        try {
            const hashedPassword = await bcrypt.hash(password, saltRounds);
    
            const result = await getALL(`INSERT INTO Usuarios (email, password, superadmin, integrantes_matricula) VALUES (?, ?, ?, ?)`, 
                [
                    email,
                    hashedPassword,
                    0,
                    matricula
                ]);
    
            console.log(result); 
    
            res.redirect('/login');
        } catch (error) {
            console.error(error);
            res.status(500).send("Error interno del servidor");
        }
    }
    
}

module.exports = PublicController;