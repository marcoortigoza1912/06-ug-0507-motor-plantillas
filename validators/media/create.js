const Joi = require("joi");

const mediaStoreSchema = Joi.object({
    titulo: Joi.string()
        .required()
        .min(5)
        .messages({
            "string.base": `"titulo" debe ser una cadena`,
            "string.empty": `"titulo" es obligatorio`,
            "string.min": `"titulo" debe tener al menos 5 caracteres`,
            "any.required": `"titulo" es obligatorio`
        }),
    matricula: Joi.string()
        .required()
        .messages({
            "string.empty": `"matricula" es obligatorio`,
            "any.required": `"matricula" es obligatorio`
        }),
    url: Joi.optional(),
    borralog: Joi.string(), 
    nombre: Joi.string()
        .required()
        .regex(/^[a-zA-Z]+$/)
        .messages({
            "string.base": `"nombre" debe ser una cadena`,
            "string.empty": `"nombre" es obligatorio`,
            "string.pattern.base": `"nombre" Solo acepta caracteres de la A a la Z`,
            "any.required": `"nombre" es obligatorio`
        }),
    texto: Joi.string()
        .required()
        .regex(/^[a-zA-Z]+$/)
        .messages({
            "string.base": `"texto" debe ser una cadena`,
            "string.empty": `"texto" es obligatorio`,
            "string.pattern.base": `"texto" Solo acepta caracteres de la A a la Z`,
            "any.required": `"texto" es obligatorio`
        }),
});

module.exports = mediaStoreSchema;
