const Joi= require("joi");
const tipomediaEditSchema = Joi.object({
    nombre: Joi.string()
        .required()
        .regex(/^[a-zA-Z\s]+$/)
        .messages({
            "string.base": `"nombre" debe ser una cadena`,
            "string.empty": `"nombre" es obligatorio`,
            "string.pattern.base": `"nombre" Solo acepta caracteres de la A a la Z`,
            "any.required": `"nombre" es obligatorio`
        }),
    
    borralog: Joi.string()
});

module.exports = tipomediaEditSchema;