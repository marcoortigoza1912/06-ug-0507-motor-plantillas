const Joi= require("joi");
const integrantesStoreSchema = Joi.object({
    matricula: Joi.string().required().min(6).messages({
        "string.base": `"matricula" debe ser una cadena`,
        "string.empty": `"matricula" es obligatorio`,
        "string.min": `"matricula" debe tener al menos 6 caracteres`,
        "any.required": `"matricula" es obligatorio`

    }),
    nombre: Joi.string().required().regex(/^[a-zA-Z]+$/).messages({
        "string.base": `"nombre" debe ser una cadena`,
        "string.empty": `"nombre" es obligatorio`,
        "string.pattern.base": `"nombre" Solo acepta caracteres de la A a la Z`,
        "any.required": `"nombre" es obligatorio`

    }),
    apellido: Joi.string().required().regex(/^[a-zA-Z]+$/).messages({
        "string.base": `"apellido" debe ser una cadena`,
        "string.empty": `"apellido" es obligatorio`,
        "string.pattern.base": `"apellido" Solo acepta caracteres de la A a la Z`,
        "any.required": `"apellido" es obligatorio`
    }),
    borralog: Joi.string()
});

/*const informarcionCreacionEjemplo={
matricula: "ug0507",
};

const {error,value}=integrantesStoreSchema.validate(
    informarcionCreacionEjemplo
);
console.log("error",error);
console.log("value",value)*/
module.exports = integrantesStoreSchema;