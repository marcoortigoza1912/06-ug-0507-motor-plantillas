const Joi = require("joi");

const integranteEditSchema = Joi.object({
    nombre: Joi.string()
        .required()
        .regex(/^[a-zA-Z\s]+$/)
        .messages({
            "string.base": `"nombre" debe ser una cadena`,
            "string.empty": `"nombre" es obligatorio`,
            "string.pattern.base": `"nombre" Solo acepta caracteres de la A a la Z`,
            "any.required": `"nombre" es obligatorio`
        }),
    apellido: Joi.string()
        .required()
        .regex(/^[a-zA-Z\s]+$/)
        .messages({
            "string.base": `"apellido" debe ser una cadena`,
            "string.empty": `"apellido" es obligatorio`,
            "string.pattern.base": `"apellido" Solo acepta caracteres de la A a la Z`,
            "any.required": `"apellido" es obligatorio`
        }),
    borralog: Joi.string()
});

module.exports = integranteEditSchema;

