
-- Crear tabla integrantes
CREATE TABLE IF NOT EXISTS "integrantes" (
    "matricula" TEXT,
    "nombre" TEXT,
    "apellido" TEXT,
    "borralog" BOOLEAN DEFAULT FALSE,
    "orden" INTEGER,
    PRIMARY KEY("matricula")
);
-- Crear tabla Tipo Media
CREATE TABLE IF NOT EXISTS "tipomedia" (
    "id" INTEGER,
    "nombre" TEXT,
    "borralog" BOOLEAN DEFAULT FALSE,
    "orden" INTEGER,
    PRIMARY KEY("id" AUTOINCREMENT)
);
-- Crear tabla Media
CREATE TABLE IF NOT EXISTS "media" (
    "id" INTEGER,
    "nombre" TEXT,
    "url" TEXT,
    "titulo" TEXT,
    "matricula" TEXT,
    "tipo_media_id" INTEGER,
    "borralog" BOOLEAN DEFAULT FALSE,
    "orden" INTEGER,
    PRIMARY KEY("id" AUTOINCREMENT),
    FOREIGN KEY("tipo_media_id") REFERENCES "tipomedia"("id"),
    FOREIGN KEY("matricula") REFERENCES "integrantes"("matricula")
);
-- Crear tabla Home
CREATE TABLE IF NOT EXISTS "home" (
	"nombre"	VARCHAR(50),
	"titulo"	VARCHAR(50),
	"src"	VARCHAR(100),
	"alt"	VARCHAR(50)
);
-- Crear tabla Usuarios
CREATE TABLE IF NOT EXISTS "usuarios" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT,
    "email" TEXT NOT NULL,
    "contraseña" TEXT NOT NULL,
    "superadmin" BOOLEAN,
    "integrantes_matricula" TEXT,
    FOREIGN KEY("integrantes_matricula") REFERENCES "integrantes"("matricula")
);
