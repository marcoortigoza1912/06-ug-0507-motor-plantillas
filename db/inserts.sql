--INSERTS TABLA INTEGRANTES
INSERT INTO integrantes (nombre, apellido, matricula, borralog) VALUES 
('Marco', 'Ortigoza', 'UG0507',FALSE),
('Elvio', 'Martinez', 'Y12858',FALSE),
('Alexis', 'Duarte', 'Y17825',FALSE),
('Gabriel', 'Garcete', 'Y23865',FALSE),
('Lennys', 'Cantero', 'Y26426',FALSE);
--SELECT* FROM integrantes

--INSERTS TABLA TIPOMEDIA
INSERT INTO tipomedia (nombre, borralog) VALUES 
('Imagen',FALSE),
('Youtube',FALSE),
('Dibujo',FALSE);
--SELECT * FROM tipomedia

INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Youtube', 'https://www.youtube.com/embed/b8-tXG8KrWs?si=qBWFL28iK9JTU3JZ', 'Video favorito de youtube', 'Y26426', 2, FALSE);
INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('imagen', '/assets/images/imagen-56.jpeg', 'Imagen representativa', 'Y26426', 1, FALSE);
INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('dibujo', '../../assets/images/imagen-57.png', 'Dibujo en Paint', 'Y26426', 3, FALSE);

INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Youtube', 'https://www.youtube.com/embed/RW75cGvO5xY', 'Video favorito de youtube', 'UG0507', 2, FALSE);
INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Imagen', '/assets/images/saturno.jpg', 'Imagen representativa', 'UG0507', 1, FALSE);
INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Dibujo', '../../assets/images/TERERE.png', 'Dibujo en paint', 'UG0507', 3, FALSE);

INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Youtube', 'https://www.youtube.com/embed/VhoHnKuf-HI', 'Video favorito de youtube', 'Y12858', 2, FALSE);
INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Imagen', '../../assets/images/melissa.jpg', 'Imagen representativa', 'Y12858', 1, FALSE);
INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Dibujo', '../../assets/images/dibujo-Elvio.png', 'Dibujo en paint', 'Y12858', 3, FALSE);

INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Youtube', 'https://www.youtube.com/embed/U1ivmi3_IeI?si=9QjM1bJzS3uUpMYk', 'Video favorito de youtube', 'Y17825', 2, FALSE);
INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Imagen', '../../assets/images/imagen-personalidad.jpg', 'Imagen representativa', 'Y17825', 1, FALSE);
INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Dibujo', '../../assets/images/dibujo-sistema-solar.png', 'Dibujo en paint', 'Y17825', 3, FALSE);

INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Youtube', 'https://www.youtube.com/embed/B4LvDiIi128?rel=0', 'Video favorito de youtube', 'Y23865', 2, FALSE);
INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Imagen', '../../assets/images/messi_pou.jpeg', 'Imagen representativa', 'Y23865', 1, FALSE);
INSERT INTO media (nombre, url, titulo, matricula, tipo_media_id, borralog) VALUES ('Dibujo', '../../assets/images/paint_garcete.jpg', 'Dibujo en paint', 'Y23865', 3, FALSE);

--SELECT * from media


--INSERT TABLA HOME
INSERT INTO home (nombre, titulo, src, alt) VALUES 
('FSOCIETY', 'Bienvenidos al grupo', '/assets/images/logo.jpeg', 'Grupo FSOCIETY');
