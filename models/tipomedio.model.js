const { getALL, db } = require('../db/conexion'); 

const TipoMedioModel = {
  getAll: async function(filters) {
    return new Promise((resolve, reject) => {
      const query = `SELECT * FROM tipomedia WHERE borralog = "0" ${filters.length ? 'AND ' + filters.join(' AND ') : ''} ORDER BY orden ASC`;
      db.all(query, [], (err, rows) => {
        if (err) {
          console.error("Error al obtener los tipos de medio:", err);
          reject(err);
        }
        resolve(rows);
      });
    });
  },

  insert: async function(nombre, borralog, orden) {
    return new Promise((resolve, reject) => {
      db.run(
        "INSERT INTO tipomedia (nombre, borralog, orden) VALUES (?, ?, ?)",
        [nombre, borralog, orden],
        (err) => {
          if (err) {
            console.error("Error al insertar en la base de datos:", err);
            reject(err);
          }
          resolve();
        }
      );
    });
  },

  getById: async function(id) {
    return new Promise((resolve, reject) => {
      db.get('SELECT * FROM tipomedia WHERE id = ?', [id], (err, row) => {
        if (err) {
          console.error("Error al obtener el tipo de medio:", err);
          reject(err);
        }
        resolve(row);
      });
    });
  },

  update: async function(id, nombre, borralog) {
    return new Promise((resolve, reject) => {
      db.run(
        "UPDATE tipomedia SET nombre = ?, borralog = ? WHERE id = ?",
        [nombre, borralog, id],
        (err) => {
          if (err) {
            console.error("Error al actualizar la base de datos:", err);
            reject(err);
          }
          resolve();
        }
      );
    });
  },

  destroy: async function(id) {
    return new Promise((resolve, reject) => {
      db.run(
        "UPDATE tipomedia SET borralog = '1' WHERE id = ?",
        [id],
        (err) => {
          if (err) {
            console.error("Error al actualizar la base de datos:", err);
            reject(err);
          }
          resolve();
        }
      );
    });
  },

  getMaxOrden: async function() {
    return new Promise((resolve, reject) => {
      db.get("SELECT MAX(orden) AS orden FROM tipomedia", [], (err, row) => {
        if (err) {
          console.error("Error al obtener el máximo orden:", err);
          reject(err);
        }
        resolve(row.orden || 0);
      });
    });
  },

  nombreduplicado: async function(nombre, id = null) {
    return new Promise((resolve, reject) => {
      const query = id ? 'SELECT COUNT(*) AS count FROM tipomedia WHERE nombre = ? AND id != ?' : 'SELECT COUNT(*) AS count FROM tipomedia WHERE nombre = ?';
      const params = id ? [nombre, id] : [nombre];
      db.get(query, params, (err, row) => {
        if (err) {
          console.error("Error al verificar la existencia de registros con el nuevo nombre:", err);
          reject(err);
        }
        resolve(row.count);
      });
    });
  }
};

module.exports = TipoMedioModel;
