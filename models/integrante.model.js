const { getALL, db} = require ("../db/conexion");

const IntegranteModel = {
    getAll: async function(filters) {
      return new Promise((resolve, reject) => {
        const borralogCondition = 'borralog = 0';
        const query = `SELECT * FROM integrantes ${filters.length ? 'WHERE ' + filters.join(' AND ') + ' AND ' + borralogCondition : 'WHERE ' + borralogCondition}`;

        db.all(query, [], (err, rows) => {
          if (err) {
            console.error("Error al obtener los integrantes:", err);
            reject(err);
          }
          resolve(rows);
        });
      });
    },
    getById: async function(idIntegrante) {
        return new Promise((resolve, reject) => {
          db.get('SELECT * FROM integrantes WHERE matricula = ?', [idIntegrante], (err, integrante) => {
            if (err) {
              console.error("Error:", err);
              reject(err);
            }
            resolve(integrante);
          });
        });
      },
    //funcion para obtener el orden de integrante 
    getMaxOrden: async function() {
      return new Promise((resolve, reject) => {
        db.get("SELECT MAX(orden) as orden FROM integrantes", [], (err, row) => {
          if (err) {
            console.error("Error al obtener el máximo orden:", err);
            reject(err);
          }
          resolve(row.orden || 0);
        });
      });
    },
  
    create: async function(matricula, nombre, apellido, borralog, orden) {
      return new Promise((resolve, reject) => {
        db.run(
          "INSERT INTO integrantes (matricula, nombre, apellido, borralog, orden) VALUES (?, ?, ?, ?, ?)",
          [matricula, nombre, apellido, borralog, orden],
          (err) => {
            if (err) {
              console.error("Error al insertar en la base de datos:", err);
              reject(err);
            }
            resolve();
          }
        );
      });
    },
  
    update: async function(idIntegrante, nombre, apellido, borralog) {
      return new Promise((resolve, reject) => {
        db.run(
          `UPDATE integrantes SET 
          nombre = ?, apellido = ?, borralog = ?
          WHERE matricula = ?`,
          [nombre, apellido, borralog, idIntegrante],
          (err) => {
            if (err) {
              console.error("Error al actualizar la base de datos:", err);
              reject(err);
            }
            resolve();
          }
        );
      });
    },
  
    delete: async function(idIntegrante) {
      return new Promise((resolve, reject) => {
        db.run(
          "UPDATE integrantes SET borralog = '1' WHERE matricula = ?",
          [idIntegrante],
          (err) => {
            if (err) {
              console.error("Error al actualizar la base de datos:", err);
              reject(err);
            }
            resolve();
          }
        );
      });
    },
  
    countByField: async function(idIntegrante) {
      return new Promise((resolve, reject) => {
        db.get("SELECT COUNT(*) AS count FROM media WHERE matricula = ? AND borralog = '0'", [idIntegrante], (err, row) => {
          if (err) {
            console.error("Error al verificar la existencia de registros asociados en la base de datos:", err);
            reject(err);
          }
          resolve(row.count);
        });
      });
    }
  };

module.exports = IntegranteModel;