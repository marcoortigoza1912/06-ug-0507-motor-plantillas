const { getALL, db } = require('../db/conexion'); 
const MediaModel = {
    getAll: async function(filters) {
    return new Promise((resolve, reject) => {
      const query = `SELECT * FROM media WHERE borralog = '0' ${filters.length ? 'AND ' + filters.join(' AND ') : ''} ORDER BY orden ASC`;
      db.all(query, [], (err, rows) => {
        if (err) {
          console.error("Error al obtener los medios:", err);
          reject(err);
        }
        resolve(rows);
      });
    });
  },

    getAllmatriculas: async function() {
    return new Promise((resolve, reject) => {
      db.all('SELECT matricula FROM integrantes', [], (err, rows) => {
        if (err) {
          console.error("Error al obtener las matrículas:", err);
          reject(err);
        }
        resolve(rows);
      });
    });
  },

    getAllTipoMedia: async function() {
    return new Promise((resolve, reject) => {
      db.all('SELECT nombre FROM tipomedia', [], (err, rows) => {
        if (err) {
          console.error("Error al obtener los tipos de media:", err);
          reject(err);
        }
        resolve(rows);
      });
    });
  },

    insert: async function(mediaData) {
    return new Promise((resolve, reject) => {
      db.run(
        'INSERT INTO media (id, nombre, url, titulo, borralog, matricula, orden, tipo_media_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
        [
          null,
          mediaData.nombre,
          mediaData.url,
          mediaData.titulo,
          mediaData.borralog,
          mediaData.matricula,
          mediaData.orden,
          mediaData.tipo_media_id
        ],
        (err) => {
          if (err) {
            console.error("Error al insertar en la base de datos:", err);
            reject(err);
          }
          resolve();
        }
      );
    });
  },

    getById: async function(idMedia) {
    return new Promise((resolve, reject) => {
      db.get('SELECT * FROM media WHERE id = ?', [idMedia], (err, row) => {
        if (err) {
          console.error("Error al obtener el medio:", err);
          reject(err);
        }
        resolve(row);
      });
    });
  },

    update: async function(idMedia, updateData) {
    return new Promise((resolve, reject) => {
      db.run(
        "UPDATE media SET url = ?, titulo = ?, borralog = ?, orden = ? WHERE id = ?",
        [updateData.url, updateData.titulo, updateData.borralog, updateData.orden, idMedia],
        (err) => {
          if (err) {
            console.error("Error al actualizar la base de datos:", err);
            reject(err);
          }
          resolve();
        }
      );
    });
  },

    destroy: async function(idMedia) {
    return new Promise((resolve, reject) => {
      db.run(   
        "UPDATE media SET borralog = '1' WHERE id = ?",
        [idMedia],
        (err) => {
          if (err) {
            console.error("Error al actualizar la base de datos:", err);
            reject(err);
          }
          resolve();
        }
      );
    });
  }
};

module.exports = MediaModel;