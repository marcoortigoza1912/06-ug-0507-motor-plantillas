//Archivo que contiene las rutas del proyecto
const express = require("express");
const router = express.Router();
//const db = require("../db/data")
const dbsqlite = require("../db/conexion");
const { getALL} = require ("../db/conexion");

require('dotenv').config({ path: '.env' });
const PublicController = require('../controllers/public/publicControler');

router.post("/login", PublicController.loginAutenticacion);

router.get("/crear/Usuario", PublicController.crear_usuario);

router.post("/crear/Usuario", PublicController.guardar_usuario)



router.get('/', PublicController.index);

router.get("/paginas/integrantes/:matricula", PublicController.index_integrantes);



router.get("/paginas/word_cloud.html", PublicController.index_worldCloud);
    
    

router.get("/paginas/curso.html", PublicController.index_Curso);
    


router.get("/login",(req,res)=>{
    res.render('login/login');
});

router.post("/login",(req, res)=>{
    const {usuario, contrasenha}= req.body;
    console.log(usuario, contrasenha);
    res.redirect("/login");
});

module.exports = router;